/////////////////////////////////////////////////////////////////////////////////////////////
//
// help
//
//    Module that displays help information about the usage of the pix console.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Dependencies
//
/////////////////////////////////////////////////////////////////////////////////////////////
var cli =   require("cli");
var type =  require("type");
var ioURI = require("io-uri");

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Constants
//
/////////////////////////////////////////////////////////////////////////////////////////////
var REPO_URL = "https://gitlab.com/api/v4/groups/pix-console/projects";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Module definition
//
/////////////////////////////////////////////////////////////////////////////////////////////
define(["pkx", "configuration"], function echo(pkx, configuration) {
    var options = {
        "text" : ""
    };

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // missingOrInvalidParameters
    //
    // Throws an error with some help info.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function compareProject(a, b) {
        if (a.name < b.name) {
            return -1;
        }
        if (a.name > b.name) {
            return 1;
        }
        return 0;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // parseParameters
    //
    // Returns an options object from the given cli parameters array.
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function parseParameters(parameters, options) {
        options = options || {};

        // create new cli interpreter
        var proc = new cli();
        proc.command("commands", "Displays a list of available commands.");

        // parse the parameters
        var p = proc.parse(parameters);
        if (p && p.commands) {
            printCommands();
        }
        else if (!p["--help"]) {
            proc.parse(parameters.concat("--help"));
        }

        return options;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // printCommands
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    function printCommands() {
        ioURI.open(REPO_URL).then(function(stream) {
            return stream.readAsJSON().then(function (projects) {
                console.log("\nAvailable commands (type \"<command> --help\" for more info):\n ");

                var table = new cli.TextTable();
                table.setHorizontalSpacing(6);

                projects = projects.sort(compareProject);
                for (var p in projects) {
                    table.addRow([projects[p].name, projects[p].description]);
                }

                table.print();

                console.log(" ");
            });
        }).catch(console.error);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // main
    //
    /////////////////////////////////////////////////////////////////////////////////////////////
    if (!configuration || !type.isArray(configuration.argv)) {
        throw new Error("This is a console app and can only be used from the console.");
    }

    parseParameters(configuration.argv, options);    
});
